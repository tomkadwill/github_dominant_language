#!/usr/bin/env ruby

Dir[File.dirname(__FILE__) + '/lib/**/*.rb'].each { |file| require file }

username = ARGV[0]

unless username
  puts "Please provide a Github username (eg: ./users_dominant_github_language.rb tomkadwill)"
  exit 1
end

user = User.new(username: username)

begin
  response = Github::UserRepositoriesClient.new(user: user).response
rescue => e
  puts "Cannot connect to Github's API, please check your internet connection"
  exit 1
end

unless response.code == "200"
  puts "Request failed with status: '#{response.code}' and message: '#{response.message}'"
  exit 1
end

serializer = Github::UserRepositoriesSerializer.new(response: response)

if serializer.invalid_username?
  puts "'#{username}' is not a valid GitHub username"
  exit 1
end

serializer.json_response.each do |repo|
  user.repositories << Repository.new(name: repo['name'], language: repo['language'])
end

puts "Dominant Language for #{username}: #{user.repositories.dominant_language}"
