class Repository
  attr_accessor :name, :language

  def initialize(name:, language:)
    @name = name
    @language = language
  end
end
