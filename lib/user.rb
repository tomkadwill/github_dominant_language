class User
  attr_accessor :username, :repositories

  def initialize(username:, repositories: Repositories.new)
    @username = username
    @repositories = repositories
  end
end
