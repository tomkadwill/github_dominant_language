require 'json'

module Github
  class UserRepositoriesSerializer
    def initialize(response:)
      @response = response
    end

    def invalid_username?
      @response.code == "404" && json_response["message"] == "Not Found"
    end

    def json_response
      JSON.parse(@response.body)
    end
  end
end
