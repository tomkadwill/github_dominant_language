require 'net/http'

module Github
  class UserRepositoriesClient
    attr_accessor :user

    def initialize(user:)
      @user = user
    end

    def response
      Net::HTTP.get_response(uri)
    end

    private

    def uri
      URI("https://api.github.com/users/#{user.username}/repos")
    end
  end
end
