class Repositories
  include Enumerable

  def initialize(*repositories)
    @repositories = repositories
  end

  def repository_names
    @repositories.map(&:language)
  end

  def dominant_language
    return "User has no repositories" unless repository_names_and_counts.any?

    max = repository_names_and_counts.max_by { |_, count| count }[1]
    repository_names_and_counts.select { |_, count| count == max}.collect {|lang, _| lang }.join(', ')
  end

  def each(&block)
    @repositories.each(&block)
  end

  def <<(repository)
    @repositories << repository
  end

  private

  def repository_names_and_counts
    repository_names.compact.group_by { |name| name }
      .map { |k, v| [k, v.count] }
  end
end
