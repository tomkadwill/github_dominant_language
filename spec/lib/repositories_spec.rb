require 'spec_helper'

describe Repositories do
  let(:repositories) { described_class.new }
  let(:rails) { Repository.new(name: 'rails', language: 'Ruby') }
  let(:node) { Repository.new(name: 'node', language: 'JavaScript') }
  let(:devise) { Repository.new(name: 'devise', language: 'Ruby') }
  let(:nil_language_project) { Repository.new(name: 'nil_language_project', language: nil) }

  describe '#dominant_language' do
    describe 'user has repositories' do
      before do
        repositories << rails
        repositories << node
        repositories << devise
      end

      it 'returns the dominant language' do
        expect(repositories.dominant_language).to eq('Ruby')
      end
    end

    describe 'user has no repositories' do
      it 'returns userful error message' do
        expect(repositories.dominant_language).to eq('User has no repositories')
      end
    end

    describe 'user has multiple dominant languages' do
      before do
        repositories << rails
        repositories << node
      end

      it 'returns multiple languages' do
        expect(repositories.dominant_language).to eq('Ruby, JavaScript')
      end
    end

    describe 'users has a repo with nil language' do
      before do
        repositories << nil_language_project
      end

      it 'does not return nil language' do
        expect(repositories.dominant_language).to eq('User has no repositories')
      end
    end
  end

  describe '#repository_names' do

    before do
      repositories << rails
      repositories << node
      repositories << devise
    end

    it 'returns array of respository names' do
      expect(repositories.repository_names).to eq(%w(Ruby JavaScript Ruby))
    end
  end
end
