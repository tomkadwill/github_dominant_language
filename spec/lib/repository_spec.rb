require 'spec_helper'

describe Repository do
  let(:name) { 'rails' }
  let(:language) { 'Ruby' }
  let(:repository) { described_class.new(name: name, language: language) }

  describe '#name' do
    it 'has a name' do
      expect(repository.name).to eq(name)
    end
  end

  describe '#language' do
    it 'has a language' do
      expect(repository.language).to eq(language)
    end
  end
end
