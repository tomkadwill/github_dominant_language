require 'spec_helper'
require 'json'

describe Github::UserRepositoriesClient do
  let(:user) { User.new(username: 'tomkadwill') }
  let(:client) { described_class.new(user: user) }
  let(:response) { client.response }

  describe '#response' do
    describe 'valid username' do
      it 'has 2XX status code' do
        VCR.use_cassette('Github Request') do
          expect(response.code).to eq('200')
        end
      end

      it 'has valid response body' do
        VCR.use_cassette('Github Request') do
          expect { JSON.parse(response.body) }.not_to raise_error
        end
      end
    end
  end
end
