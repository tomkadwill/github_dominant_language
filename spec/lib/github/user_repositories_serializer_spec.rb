require 'spec_helper'
require 'json'

describe Github::UserRepositoriesSerializer do
  let(:user) { User.new(username: username) }
  let(:response) { Github::UserRepositoriesClient.new(user: user).response }
  let(:serializer) { described_class.new(response: response) }

  describe '#json_response' do
    let(:username) { 'tomkadwill' }
    let(:json_response) { serializer.json_response }

    it 'returns the hash representation of the response' do
      VCR.use_cassette('Github Request') do
        expect(json_response.first['name']).to eq('AlertSweeper')
        expect(json_response.first['language']).to eq('Swift')
      end
    end
  end

  describe '#invalid_username?' do
    describe 'with valid username' do
      let(:username) { 'tomkadwill' }

      it 'returns false' do
        VCR.use_cassette('Github Request') do
          expect(serializer.invalid_username?).to eq(false)
        end
      end
    end

    describe 'with valid username' do
      let(:username) { 'tomkadwilllll' }

      it 'returns true' do
        VCR.use_cassette('Github Request error') do
          expect(serializer.invalid_username?).to eq(true)
        end
      end
    end
  end
end
