require 'spec_helper'

describe User do
  let(:username) { 'David' }
  let(:repositories) { double(:repositories) }
  let(:user) { described_class.new(username: username, repositories: repositories) }

  describe '#username' do
    it 'returns username' do
      expect(user.username).to eq(username)
    end
  end

  describe '#repositories' do
    it 'returns repositories collection' do
      expect(user.repositories).to eq(repositories)
    end
  end
end
