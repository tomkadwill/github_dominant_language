require 'vcr'

Dir[File.dirname(File.expand_path("../", __FILE__)) + '/lib/**/*.rb'].each { |file| require file }

VCR.configure do |config|
  config.cassette_library_dir = "fixtures/vcr_cassettes"
  config.hook_into :webmock # or :fakeweb
end
