## Github Dominant Language Project

This project is a command line tool which allows you to determine a Github users' dominant language. The project
can be run as follows:

    $ users_dominant_github_language.rb tomkadwill

The `users_dominant_github_language.rb` script accepts 1 argument, a Github username.

### Testing

This project uses Rspec for unit testing. You can run the specs from the project root directory:

    $ rspec spec/

### Dependencies

This project has been written for Ruby 2.3.1

### Design Decisions
* There are github client gems available on RubyGems. I chose to write my own class because I don't want to
  Add a dependency when I'm only using 1 of the GitHub API endpoints. If this project were to grow to include other
  parts of GitHub's API then I would recommend removing GithubUserRepositoriesClient and using a 3rd party gem
  instead.
